package exercise.agenda.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.persistence.Entity;

@Entity
public class RecurringEvent extends Event {

	public enum Period { HOURLY, DAILY, WEEKLY, BIWEEKLY, MONTHLY, ANNUAL}
	
	Period period;

	public Period getPeriod() {
		return period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}

	@Override
	public String toString(){
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		return String.format("{id:%d, title:%s, description:%s, startTime:%s, endTime:%s, calendar:%d, period:%s }"
				, this.id, this.title, this.description, df.format(this.startTime)
				, df.format(this.endTime), this.calendar.getId(), this.period);	}	
}
