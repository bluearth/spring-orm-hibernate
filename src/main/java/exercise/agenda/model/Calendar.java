package exercise.agenda.model;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
public class Calendar {

	@Id
	@GeneratedValue(
			strategy=GenerationType.SEQUENCE, 
			generator="hilo_sequence_generator")
	@GenericGenerator(
            name = "hilo_sequence_generator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @Parameter(name = "sequence_name", value = "SeqCalendar_calendarId"),
                    @Parameter(name = "initial_value", value = "10"),
                    @Parameter(name = "increment_size", value = "1"),
                    @Parameter(name = "optimizer", value = "hilo")
            })	
	public Long getId() {return id;}
	@Column(length=50,nullable=false)
	public String getTitle() {return title;}
	@ManyToOne
	public Person getOwner() {return owner;}
	@OneToMany(mappedBy="calendar")
	public Collection<Event> getEvents() {return events;}
	
	
	
	public void setId(Long id) {this.id = id;}
	public void setTitle(String title) {this.title = title;}
	public void setOwner(Person owner) {this.owner = owner;}
	public void setEvents(Collection<Event> events) {this.events = events;}

	@Override
	public String toString(){
		return String.format("{id:%d, title:%s, owner:%d}"
				, this.id, this.title, this.owner.getId());
	}	
	
	Long id;
	String title;
	Person owner;
	Collection<Event> events;
	
}
