package exercise.agenda.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
public class Person {
	@Id
	@GeneratedValue(
			strategy=GenerationType.SEQUENCE, 
			generator="hilo_sequence_generator")
	@GenericGenerator(
            name = "hilo_sequence_generator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @Parameter(name = "sequence_name", value = "SeqPerson_personId"),
                    @Parameter(name = "initial_value", value = "10"),
                    @Parameter(name = "increment_size", value = "1"),
                    @Parameter(name = "optimizer", value = "hilo")
            })	
	public Long getId() { return id;}
	public void setId(Long id) {this.id = id;}
	public String getName() {return name;}
	public void setName(String name) {this.name = name;}
	public Date getDob() {return dob;}
	public void setDob(Date dob) {this.dob = dob;}
	
	Long id;
	String name;
	Date dob;	
	
	@Override
	public String toString(){
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		return String.format("{id:%d, name:%s, dob:%s}", this.id, this.name, df.format(this.dob));
	}
}
