package exercise.agenda.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)  
public class Event {

	@Id
	@GeneratedValue(
			strategy=GenerationType.SEQUENCE, 
			generator="hilo_sequence_generator")
	@GenericGenerator(
            name = "hilo_sequence_generator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @Parameter(name = "sequence_name", value = "SeqCalendar_calendarId"),
                    @Parameter(name = "initial_value", value = "10"),
                    @Parameter(name = "increment_size", value = "1"),
                    @Parameter(name = "optimizer", value = "hilo")
            })		
	public Long getId() {return id;}
	public String getTitle() {return title;}
	public String getDescription() {return description;}
	public Date getStartTime() {return startTime;}
	public Date getEndTime() {return endTime;}
	@ManyToOne
	public Calendar getCalendar() {return calendar;}

	public void setId(Long id) {this.id = id;}
	public void setTitle(String title) {this.title = title;}
	public void setDescription(String description) {this.description = description;}
	public void setStartTime(Date startTime) {this.startTime = startTime;}
	public void setEndTime(Date endTime) {this.endTime = endTime;}
	public void setCalendar(Calendar calendar) {this.calendar = calendar;}
	
	@Override
	public String toString(){
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		return String.format("{id:%d, title:%s, description:%s, startTime:%s, endTime:%s, calendar:%d }"
				, this.id, this.title, this.description, df.format(this.startTime)
				, df.format(this.endTime), this.calendar.getId());
	}	

	Long id;
	String title;
	String description;
	Date startTime;
	Date endTime;
	Calendar calendar;
	
	
}
