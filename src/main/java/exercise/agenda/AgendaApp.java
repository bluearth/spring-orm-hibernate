package exercise.agenda;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AgendaApp {

	static Logger logger = LoggerFactory.getLogger(AgendaApp.class);
	
	public static void main(String[] args) throws Exception {

		ApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
		
		SessionFactory sessionFactory = context.getBean(SessionFactory.class);
		
		
		
		((AbstractApplicationContext) context).close();
		
		
	}

}
